#ifndef WEBPREPFILE_H
#define WEBPREPFILE_H

#include <QDeclarativeListProperty>


class WPFBase : public QObject {
	Q_OBJECT
	Q_PROPERTY(QDeclarativeListProperty<WPFBase> children READ children)
	Q_PROPERTY(QString type READ type WRITE setType)
	Q_CLASSINFO("DefaultProperty", "children")
public:
	WPFBase(QObject *parent = 0);


	QDeclarativeListProperty<WPFBase> children();
	QString type() const;
	void setType(const QString &);
	
	QString getMetaString(const char *property);

//private:
	QList<WPFBase *> prv_chldrn;
	QString prv_type;
};

#endif // WEBPREPFILE_H