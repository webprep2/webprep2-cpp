#include <QtCore/QtCore>
#include <QtDeclarative>
#include "webprepfile.h"

void processTaskList(QList<WPFBase*> taskList, QDeclarativePropertyMap *statePM);


QTextStream errorS(stderr,QIODevice::WriteOnly);
QTextStream outS(stdout,QIODevice::WriteOnly);
bool safeMode=false;
QString fileName ="";


void ensureTargetPath(QString targetPath){
	if(!(QDir(targetPath).exists())){
		QString absTargetPath = (QDir(targetPath).absolutePath());
		QDir::root().mkpath(absTargetPath);
	}
}

QString createContent(QString dir, QString fileName){
	
	QDeclarativeEngine engine;
	engine.setBaseUrl(QUrl::fromLocalFile(dir));
	QDeclarativeComponent component(&engine, QUrl::fromLocalFile(fileName));
	if(component.errors().length() > 0)
		qDebug() << component.errors();
	QObject *comObj = component.create(); 

	
	return comObj->property("result").toString();	
};

QStringList* createMultiContent(QString dir, QString fileName, QList<int> pageRanges){
	
	
	QStringList* ret = new QStringList[2];
	QStringList& content = ret[0];
	QStringList& meta = ret[1];
	
	//ranges are listed as: firstPage,lastPage
	//pageRanges has to be monotonically increasing
	//no page may be part of two Ranges
	//if the variable ranges is odd, the last range goes until the last avialable page
	//empty list means deliver no pages at all
	//a list containig just 0 means: deliver all pages
	int ranges = pageRanges.length();
	if(ranges == 0)
		return ret;
	QListIterator<int> rangeIt(pageRanges);
	//no need for hasNext() here
	int nextRangeBorder = rangeIt.next();
	int page = -1; 
	bool outputPage  = false;
	
	QDeclarativeEngine engine;
	engine.setBaseUrl(QUrl::fromLocalFile(dir));
	QDeclarativeComponent component(&engine, QUrl::fromLocalFile(fileName));
	if(component.errors().length() > 0)
		qDebug() << component.errors();
	QObject *comObj = component.create();
	
	do {
		++page;
		//Note: we will past the end of a range and check afterwards
		if(nextRangeBorder != -1 && page > nextRangeBorder){
			outputPage = false;
			if(rangeIt.hasNext())
				nextRangeBorder = rangeIt.next();
			else//past last end of range and no other range
				break;
		}
		//after &&: handels last page
		if(page == nextRangeBorder && !outputPage){
			outputPage = true;
			if(rangeIt.hasNext())
				nextRangeBorder = rangeIt.next();
			else
				nextRangeBorder = -1;
		}
		comObj->setProperty("getPage",page);
		content << (comObj->property("result").toString());
		meta << (comObj->property("meta").toString());
	 
	} while (comObj->property("moreContent").toBool());
	
	return ret;	
};


void processWebprepfile(QString fileName, QDeclarativePropertyMap *statePM){
	
	QDeclarativeEngine engine;
	engine.setBaseUrl(QUrl::fromLocalFile(statePM->value("sourcePath").toString()));
	outS << "Webprepfile Base URL: " << engine.baseUrl().toString() << endl;
	QDeclarativeComponent component(&engine, QUrl::fromLocalFile(fileName));
	if(component.errors().length() > 0)
		qDebug() << component.errors();
	WPFBase *wpfRoot = qobject_cast<WPFBase *>(component.create());
	
	
	QDeclarativePropertyMap *subStatePM = new QDeclarativePropertyMap();
	subStatePM->insert("sourcePath", statePM->value("sourcePath"));
	subStatePM->insert("targetPath", 
		QVariant(statePM->value("targetPath").toString() + wpfRoot->getMetaString("target")
	));
	
	
	QList<WPFBase*> taskList = wpfRoot->prv_chldrn;
	processTaskList(taskList,subStatePM);
}

void processDirectory(WPFBase* wpfBase,QDeclarativePropertyMap *statePM){
	QDeclarativePropertyMap *subStatePM = new QDeclarativePropertyMap();
	subStatePM->insert("sourcePath", 
		QVariant(statePM->value("sourcePath").toString() + wpfBase->getMetaString("source")
	));
	subStatePM->insert("targetPath", 
		QVariant(statePM->value("targetPath").toString() + wpfBase->getMetaString("target")
	));
	
	QList<WPFBase*> taskList = wpfBase->prv_chldrn;
	processTaskList(taskList,subStatePM);
}

void postProcessFile(WPFBase* wpfBase,QDeclarativePropertyMap *statePM){
	if(wpfBase->getMetaString("postProcess") == "" || safeMode)
		return;
	QProcess::execute(wpfBase->getMetaString("postProcess"),
		QStringList(
		statePM->value("targetPath").toString() + wpfBase->getMetaString("target"))
	<< statePM->value("targetPath").toString() + wpfBase->getMetaString("ppOut"));
	
}

void processFile(WPFBase* wpfBase,QDeclarativePropertyMap *statePM){
	QString content = createContent(statePM->value("sourcePath").toString(),wpfBase->getMetaString("source"));
	
	QFile target(statePM->value("targetPath").toString() + wpfBase->getMetaString("target"));
	ensureTargetPath(QFileInfo(target).dir().absolutePath());
	
	if (!target.open(QIODevice::WriteOnly | QIODevice::Text))
		return;

	QTextStream write(&target);
	write << content << endl;
	
	target.close();
}

void multiFile(WPFBase* wpfBase,QDeclarativePropertyMap *statePM){
	QList<int> pageRanges;
	//for now we will only one range
	//if/when list<int> ist supported in QML
	//a list "ranges" will be recognized, too
	if(wpfBase->property("first_page").isValid ())
		pageRanges << wpfBase->property("first_page").toInt();
	else if (wpfBase->property("last_page").isValid ())
		pageRanges << 0;
	if(wpfBase->property("last_page").isValid () && wpfBase->property("last_page").toInt() >= 0)
		pageRanges << wpfBase->property("last_page").toInt();

	QStringList* both = createMultiContent(statePM->value("sourcePath").toString(),wpfBase->getMetaString("source"),pageRanges);
	QStringList& contents = both[0];
	QStringList& meta = both[1];
	
	for(int i = 0; i < contents.length();i++){
	
		wpfBase->setProperty("getPage",QString::number(i));
		wpfBase->setProperty("getMeta",meta[i]);
		QFile target(statePM->value("targetPath").toString() + wpfBase->getMetaString("target"));
		ensureTargetPath(QFileInfo(target).dir().absolutePath());
	
		if (!target.open(QIODevice::WriteOnly | QIODevice::Text))
			continue;

		QTextStream write(&target);
		write << contents[i] << endl;
	
		target.close();
	}
}


bool copyFile(WPFBase* wpfBase,QDeclarativePropertyMap *statePM){
	QString tg =  statePM->value("targetPath").toString() + wpfBase->getMetaString("target");
	QString src = statePM->value("sourcePath").toString() + wpfBase->getMetaString("source");
	QFile tgfile(tg);
	if(tgfile.exists())
		tgfile.remove();
	return QFile::copy(src,tg);
}

void processTaskList(QList<WPFBase*> taskList, QDeclarativePropertyMap *statePM){
	for(int i = 0; i< taskList.length();i++){
		if(taskList[i]->type() == "dir")
			processDirectory(taskList[i],statePM);
		else if (taskList[i]->type() == "qml_write"){
			processFile(taskList[i],statePM);
			postProcessFile(taskList[i],statePM);
		}
		else if (taskList[i]->type() == "bin_copy"){
			copyFile(taskList[i],statePM);
			postProcessFile(taskList[i],statePM);
		}
		else if (taskList[i]->type() == "wpfile"){
			processWebprepfile(taskList[i]->getMetaString("source"),statePM);
		}
		else if (taskList[i]->type() == "multi_file"){
			multiFile(taskList[i],statePM);
		}
	}
}

void setOptions(){
	//for now file names may noch start with "-"
	QStringList args = QCoreApplication::arguments(); 
	for (int i = 1;i< args.length();i++){
		if(args[i].startsWith("-")){
			for(int j=1;j<args[i].length();j++)
				switch(args[i][j].toAscii()){
				case 's':
					safeMode = true;
					outS << "safeMode";
					break;
				default :
					break;
				}
		} else 
			fileName = args[i];
	}
	fileName = fileName == ""?  "WFile.qml" : fileName;
}


int main(int argc, char **argv) {
	QCoreApplication app(argc, argv);
	/*the source file shall be coded in UTF-8
	and therefor all C-Strings should be interpretet as UTF-8*/
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
	
	qmlRegisterType<WPFBase>("webprep2", 1,0, "WPFBase");
	
	
	
	QDeclarativePropertyMap *statePM = new QDeclarativePropertyMap();
	statePM->insert("sourcePath", QVariant(QDir::currentPath()+"/"));
	statePM->insert("targetPath", QVariant(QDir::currentPath()+"/"));


	setOptions();

	processWebprepfile(fileName,statePM);
	
	//outS << createContent(fileName)  << endl;
	
	
	
	return 0;
}
 
