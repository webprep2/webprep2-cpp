#include "webprepfile.h"
#include <QMetaProperty>

WPFBase::WPFBase(QObject *parent) : QObject(parent){
}

QDeclarativeListProperty<WPFBase> WPFBase::children() {
	return QDeclarativeListProperty<WPFBase>(this, prv_chldrn);
}

QString WPFBase::type() const {
	return prv_type;
};

void WPFBase::setType(const QString & val){
	prv_type = val;
};

QString WPFBase::getMetaString(const char *prop){
// 	const QMetaObject *mtComObj = metaObject();
// 	if(mtComObj->indexOfProperty(prop) >= 0)
// 		return mtComObj->property(mtComObj->indexOfProperty(prop)).read(this).toString();
// 	else
// 		return "";
	return property(prop).toString();
}